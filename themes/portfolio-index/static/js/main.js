import SiteMenu from './site-menu.js'

const documentReady = async () => {
	const $body = document.querySelector('body')
	$body.classList.remove('is-noJs')

	if (document.readyState === 'complete') {
		try {
			await loadFonts()
		} catch(error) {
			console.log('Error loading site', error)
		}
		$body.classList.add('is-fontLoaded')

		initWebsite()
	}
}

const loadFonts = () => {
	const fontUrl = 'https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap'
	return new Promise(resolve => {
		const $fontLink = document.createElement('link')
		$fontLink.setAttribute('href', fontUrl)
		$fontLink.setAttribute('rel', 'stylesheet')
		$fontLink.onload = () => resolve()
		document.querySelector('body').append($fontLink)
	})
}

/* a gallery is in fact two, onefor the media, the other for the text
	 content */
const initGalleries = () => {
	const $gallery = document.querySelector('.Gallery')
	if (!$gallery) return

	let galleryFlickity = new Flickity('.Gallery', {
		imagesLoaded: true,
		cellSelector: '.Gallery-slide',
		/* asNavFor: '.GalleryControls', */
		hash: true,
		pageDots: false,
		prevNextButtons: false,
	})

	const $galleriesControls = document.querySelector('.GalleryControls')
	let galleryControlsFlickity
	if ($galleriesControls) {
		galleryControlsFlickity = new Flickity('.GalleryControls', {
			adaptiveHeight: true,
			asNavFor: '.Gallery',
			pageDots: false,
			prevNextButtons: false,
			draggable: false
		})
	}

	const $buttonGroup = document.createElement('aside')
	$buttonGroup.classList.add('GalleryButtons')

	const $buttonNext = document.createElement('button')
	$buttonNext.classList.add(
		'GalleryButtons-button',
		'GalleryButtons-button--next'
	)
	$buttonNext.innerText = '→'
	$buttonNext.addEventListener('click', function() {
		galleryFlickity.next()
	})

	const $buttonPrev = document.createElement('button')
	$buttonPrev.classList.add(
		'GalleryButtons-button',
		'GalleryButtons-button--prev'
	)
	$buttonPrev.innerText = '←'
	$buttonPrev.addEventListener('click', function(event) {
		galleryFlickity.previous()
	})

	$buttonGroup.append($buttonPrev)
	$buttonGroup.append($buttonNext)
	$gallery.append($buttonGroup)
}

const initWebsite = () => {
	customElements.define('site-menu', SiteMenu)
	console.log('Hello Isa!')
	initGalleries()
}

document.onreadystatechange = documentReady
