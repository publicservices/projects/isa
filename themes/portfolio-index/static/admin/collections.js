import paintings from './model-paintings.js'
import projects from './model-projects.js'
import pages from './model-pages.js'
import datas from './model-datas.js'
import configs from './model-configs.js'

export default [
	paintings,
	projects,
	pages,
	datas,
	configs
]
