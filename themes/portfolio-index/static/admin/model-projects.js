import title from './field-title.js'
import featuredImage from './field-featured-image.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import datePublished from './field-date-published.js'

const projects	= {
	name: 'projects',
	label: 'Projects',
	label_singular: 'Project',
	folder: 'content/projects',
	format: 'toml-frontmatter',
	create: true,
	slug: '{{title}}',
	editor: {
		preview: false
	},
	fields: [
		title,
		slug,
		draft,
		datePublished,
		featuredImage,
		body
	]
}

export default projects
