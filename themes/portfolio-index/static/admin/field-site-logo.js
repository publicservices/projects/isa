export default {
    label: 'Site logo',
    name: 'logo',
    widget: 'image',
    required: false,
    hint: 'The image, used on the homepage/site as the logo'
}
