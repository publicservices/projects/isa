+++
title = "A project"
date_published = "2020-12-01"
image = "/media/uploads/6.jpg"
+++
On this project, I will write about some stuff I like.

# First Title, about things

![Hello alt](/media/uploads/903c0c41-7032-4a69-93f4-bc1cd3897aad.jpg "Hello title")

And here some more text!



# And now, some examples of videos and audio

And a Youtube video?

<iframe width="560" height="315" src="https://www.youtube.com/embed/5Da4D-1BJlQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

And a sound cloud track maybe?

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1039618432&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/matt-bowdler-music" title="Matt Bowdler Music" target="_blank" style="color: #cccccc; text-decoration: none;">Matt Bowdler Music</a> · <a href="https://soundcloud.com/matt-bowdler-music/sets/2020-demo-reel" title="2020 Demo Reel" target="_blank" style="color: #cccccc; text-decoration: none;">2020 Demo Reel</a></div>



We can also use a very wide image;



![](/media/uploads/img_20160802_163413.jpg)



It goes to the edges of the screen.